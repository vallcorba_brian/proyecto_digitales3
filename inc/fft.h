#ifndef _FFT_H_
#define _FFT_H_

/** \addtogroup FFT
 ** @{ */

/*==================[inclusions]=============================================*/

#include "arm_math.h"
#include "arm_const_structs.h"

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
extern "C" {
#endif

/*==================[macros]=================================================*/
#define FFT_MODE 0  /*If defined at 1, it'll realize the Inverse FFT*/
#define FFT_SIZE 2048
#define DO_BIT_REVERSE 1

/*==================[typedef]================================================*/

/*==================[external data declaration]==============================*/

/*==================[external functions declaration]=========================*/


void adc_Float_Conv(uint32_t* adcInt, float32_t* out, uint16_t len, float32_t Vref, uint16_t dma_number);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/** @} doxygen end group definition */
/*==================[end of file]============================================*/
#endif /* #ifndef _FFT_H_ */
