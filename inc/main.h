#ifndef _MAIN_H_
#define _MAIN_H_

/*==================[inclusions]=============================================*/

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
extern "C" {
#endif

/*==================[macros]=================================================*/
/**@brief Power of 2, to get better resolution*/
#define N_SAMPLES  100
/**@brief ADC reference voltage */
#define V_REF	3.3

/*==================[typedef]================================================*/

/*==================[external data declaration]==============================*/

/*==================[external functions declaration]=========================*/

/**
 * @brief   Application entry point
 * @return  main() function should never return
 */
int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[end of file]============================================*/
#endif /* #ifndef _MAIN_H_ */
