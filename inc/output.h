#ifndef _OUTPUT_H_
#define _OUTPUT_H_

/** \addtogroup OUTPUT
 ** @{ */

/*==================[inclusions]=============================================*/
#include "arm_math.h"
#include "lcd.h"
#include "ciaaUART.h"
/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
extern "C" {
#endif

/*==================[macros]=================================================*/


/*==================[typedef]================================================*/

/*==================[external data declaration]==============================*/

/*==================[external functions declaration]=========================*/
/**@brief displays required tuning on LCD
 */
bool displayTuningNeeded(float32_t min ,float32_t frequency);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/** @} doxygen end group definition */
/*==================[end of file]============================================*/
#endif /* #ifndef _OUTPUT_H_ */
