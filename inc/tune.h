#ifndef _TUNE_H_
#define _TUNE_H_

/** \addtogroup TUNING
 ** @{ */

/*==================[inclusions]=============================================*/

#include "arm_math.h"
#include "board.h"

/*==================[cplusplus]==============================================*/



/*==================[macros]=================================================*/

#define GUITAR_TUNE_RANGE 3

/*==================[typedef]================================================*/

/*==================[external data declaration]==============================*/

/*==================[external functions declaration]=========================*/
/**@brief	guitar tuning function
 * @return	returns the minimum value*/
extern float32_t guitarTune(float32_t frequency ,float32_t* fftOutput, uint16_t Sz);


/*==================[cplusplus]==============================================*/



/** @} doxygen end group definition */
/*==================[end of file]============================================*/
#endif /* #ifndef _TUNE_H_ */
