/** \addtogroup fft
 ** @{ */

/*==================[inclusions]=============================================*/
#include "arm_math.h"
#include "board.h"
/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/




/*==================[internal data definition]===============================*/



/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/


/*==================[external functions definition]==========================*/

void adc_Float_Conv(uint32_t* adcInt, float32_t* out, uint16_t len, float32_t Vref, uint16_t dma_number)
{
	/**/
	float32_t aux;


	aux = (float32_t) ADC_DR_RESULT(adcInt[0]);
	aux = aux / len;
	aux = aux * Vref;
	out[dma_number] = aux;

}

/** @} doxygen end group definition */

/*==================[end of file]============================================*/
