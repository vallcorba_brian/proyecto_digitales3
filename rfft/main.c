/*==================[inclusions]=============================================*/
#include "main.h"

#include "lcd.h"
#include "board.h"
#include "arm_math.h"
#include "arm_const_structs.h"
#include "ff.h"
#include "fft.h"

#include "tune.h"
#include "output.h"
#include "FreeRTOSConfig.h"
#include "ciaaUART.h"

#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "queue.h"

/*==================[macros and definitions]=================================*/

#define TASK_STACK_SIZE  256  // VER TAMAÑO DE LA TAREA

/*==================[internal data declaration]==============================*/

static uint8_t rx_bytes;
static char rx_buf, rx_char;

/*==================[internal functions declaration]=========================*/

static void initHardware(void);

/*==================[internal data definition]===============================*/

static uint8_t dma_ch_adc;

static uint32_t dma_buf[N_SAMPLES];

static float32_t fftInput[FFT_SIZE];	/*Input to fft function*/
static float32_t fftOutput[FFT_SIZE];	/*magnitude of fft function output*/

arm_rfft_fast_instance_f32 rfft_instance;


static SemaphoreHandle_t dmaSemBin;
static  SemaphoreHandle_t fftSemBin;
static SemaphoreHandle_t dataReadySem;


/*==================[external data definition]===============================*/


/*==================[internal functions definition]==========================*/

static void initHardware(void) {
	Board_Init();

	LCD_Init();

	SystemCoreClockUpdate();

	/*===================== ADC Configuration ================================*/
	ADC_CLOCK_SETUP_T adc_setup;
	Chip_ADC_Init(LPC_ADC0, &adc_setup);
	/* Minimum Sample Rate: 73k. Our sample rate is 8k. We take 10 samples,
	 * and adopt the sample rate as 80k */
	Chip_ADC_SetSampleRate(LPC_ADC0, &adc_setup, 2048*N_SAMPLES);/*102400*/
	Chip_ADC_SetBurstCmd(LPC_ADC0, ENABLE);

	/*Enable ADC0 CH1 and its interrupt*/
	Chip_ADC_EnableChannel(LPC_ADC0, ADC_CH1, ENABLE);
	Chip_ADC_Int_SetChannelCmd(LPC_ADC0, ADC_CH1, ENABLE);

	/*===================== DMA Controller Configuration ================================*/
	Chip_GPDMA_Init(LPC_GPDMA);
	dma_ch_adc = Chip_GPDMA_GetFreeChannel(LPC_GPDMA, GPDMA_CONN_ADC_0);

	/*Enable DMA interrupt in the NVIC*/
	NVIC_ClearPendingIRQ(DMA_IRQn);
	NVIC_EnableIRQ(DMA_IRQn);

	/*===================== UART Configuration ================================*/

	/* EDU-CIAA USART configuration */
	Chip_SCU_PinMuxSet(2, 3, SCU_MODE_FUNC2);
	Chip_SCU_PinMuxSet(2, 4, SCU_MODE_INBUFF_EN | SCU_MODE_FUNC2);

	/* USART3 peripheral configuration */
	Chip_UART_Init(LPC_USART3);
	Chip_UART_SetBaud(LPC_USART3, 9600);
	Chip_UART_TXEnable(LPC_USART3);

	/* Enable USART3 RBR interrupt */
	Chip_UART_IntEnable(LPC_USART3, UART_IER_RBRINT);

	/* Enable USART3 interrupt in the NVIC */
	NVIC_ClearPendingIRQ(USART3_IRQn);
	NVIC_EnableIRQ(USART3_IRQn);

	/*UART	2 for debugging*/
	/* EDU-CIAA USART configuration */
	Chip_SCU_PinMuxSet(7, 1, SCU_MODE_FUNC6);  // [UM:17.4.1]
	Chip_SCU_PinMuxSet(7, 2, SCU_MODE_INBUFF_EN | SCU_MODE_FUNC6);

	/* USART3 peripheral configuration */
	Chip_UART_Init(LPC_USART2);
	Chip_UART_SetBaud(LPC_USART2, 9600);
	Chip_UART_TXEnable(LPC_USART2);

	/* Enable USART3 RBR interrupt */
	Chip_UART_IntEnable(LPC_USART2, UART_IER_RBRINT);

	/* Enable USART3 interrupt in the NVIC */
	NVIC_ClearPendingIRQ(USART2_IRQn);
	NVIC_EnableIRQ(USART2_IRQn);

	/*===================== SPI Configuration ================================*/

	Board_SSP_Init(LPC_SSP1);
	Chip_SSP_Init(LPC_SSP1);
	Chip_SSP_Enable(LPC_SSP1);

}

void startDMATransfer(void) {
	Chip_GPDMA_Transfer(LPC_GPDMA, dma_ch_adc,
			GPDMA_CONN_ADC_0, (uint32_t) &dma_buf,
			GPDMA_TRANSFERTYPE_P2M_CONTROLLER_DMA, N_SAMPLES);
}


/*==================[external functions definition]==========================*/


/* DMA interrupt handler triggered after N_SAMPLES conversions of ADC0_1 */
void DMA_IRQHandler(void) {
	portBASE_TYPE taskWoken;
	if (Chip_GPDMA_Interrupt(LPC_GPDMA, dma_ch_adc) == SUCCESS)
	{
		xSemaphoreGiveFromISR(dmaSemBin , &taskWoken);
	}
	portEND_SWITCHING_ISR(taskWoken);
}


static void task_FFT()
{
	/*	Once the data has been read and converted to float, this task calculates
	 * 	the float point FFT.
	 */
	uint16_t i;

	xSemaphoreTake(fftSemBin , 0);
	while (1)
	{
		xSemaphoreTake(fftSemBin , portMAX_DELAY);

//		arm_cfft_f32(&arm_cfft_sR_f32_len2048 ,fftInput ,FFT_MODE ,DO_BIT_REVERSE);
		arm_rfft_fast_f32(&rfft_instance,fftInput,fftOutput,FFT_MODE);

		/*FFT Module */
		arm_cmplx_mag_f32(fftOutput, fftInput, FFT_SIZE);

		for (i = FFT_SIZE/2; i < FFT_SIZE; i++)
		{
			fftOutput[i] = 0;
		}

		xSemaphoreGive(dataReadySem);
	}
}

static void task_DMA()
{
	uint16_t i= 0;
	xSemaphoreTake(dmaSemBin , 0);
	while (1)
	{
		/* After the DMA transfers N_SAMPLES values from ADC0_CH1 to
		 * dma_buf[], copy them to adc_buf[] and start a new DMA transfer */

		xSemaphoreTake(dmaSemBin , portMAX_DELAY);
		if (i == FFT_SIZE)		/*¿¿¿¿¿ 2*FFT_SIZE?????*/
		{
			i = 0;
			xSemaphoreGive(fftSemBin);
		}
		else
		{
			adc_Float_Conv(dma_buf, fftInput, FFT_SIZE, V_REF, i);
			i++;
			startDMATransfer();
		}
	}
}


static void taskmain(void * a) {
	float32_t  maxValue;
	uint32_t maxIndex;

	xSemaphoreTake(dataReadySem, 0);
	startDMATransfer();
	arm_rfft_fast_init_f32(&rfft_instance,FFT_SIZE);

	while (1) {


		/*Wait for fft and SD tasks*/
		xSemaphoreTake(dataReadySem, portMAX_DELAY);

		fftOutput[0] = 0;

		arm_max_f32(fftOutput, FFT_SIZE, &maxValue, &maxIndex);

		startDMATransfer();
	}
}

int main(void) {
	initHardware();

	xTaskCreate(task_DMA, (const char*)"task_DMA", TASK_STACK_SIZE, 0, tskIDLE_PRIORITY+1, 0);
	xTaskCreate(task_FFT, (const char * )"task_FFT", TASK_STACK_SIZE, 0, tskIDLE_PRIORITY+1, 0);
	xTaskCreate(taskmain, (const char * )"taskmain", TASK_STACK_SIZE, 0, tskIDLE_PRIORITY+1, 0);



	dmaSemBin = xSemaphoreCreateBinary();
	fftSemBin = xSemaphoreCreateBinary();
	dataReadySem = xSemaphoreCreateBinary();


	vTaskStartScheduler();

	while (1) {
	}

}

/** @} doxygen end group definition */

/*==================[end of file]============================================*/
