/*==================[inclusions]=============================================*/
#include "main.h"

#include "lcd.h"
#include "board.h"
#include "arm_math.h"
#include "arm_const_structs.h"
#include "ff.h"
#include "fft.h"

#include "tune.h"
#include "output.h"
#include "FreeRTOSConfig.h"
#include "ciaaUART.h"

#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "queue.h"

/*==================[macros and definitions]=================================*/

#define TASK_STACK_SIZE  256  //  TAMAÑO DE LA TAREA
#define NOISE_LEVEL	2
#define FREQUENCY_RES 30


/*==================[internal data declaration]==============================*/


/*==================[internal functions declaration]=========================*/

static void initHardware(void);
static int8_t find_key(void);
static uint8_t vert_sweep(void);
static uint8_t hor_sweep(uint8_t index);

/*==================[internal data definition]===============================*/

static uint8_t dma_ch_adc;

static uint32_t dma_buf[N_SAMPLES];

static float32_t fftInput[2*FFT_SIZE];	/*Input to fft function*/
static float32_t fftOutput[FFT_SIZE];	/*magnitude of fft function output*/

static uint8_t dtmf_standard[] = {'1', '2', '3', 'A', '4', '5', '6', 'B', '7', '8', '9', 'C', '*', '0','#','D'};

static SemaphoreHandle_t dmaSemBin;
static  SemaphoreHandle_t fftSemBin;
static SemaphoreHandle_t dataReadySem;


/*==================[external data definition]===============================*/


/*==================[internal functions definition]==========================*/

static void initHardware(void) {
	Board_Init();

	LCD_Init();

	SystemCoreClockUpdate();

	/*===================== ADC Configuration ================================*/
	ADC_CLOCK_SETUP_T adc_setup;
	Chip_ADC_Init(LPC_ADC0, &adc_setup);
	/* Minimum Sample Rate: 73k. Our sample rate is 8k. We take 10 samples,
	 * and adopt the sample rate as 80k */
	Chip_ADC_SetSampleRate(LPC_ADC0, &adc_setup, FFT_SIZE*N_SAMPLES);/*Sampling rate: FFT_SIZE*2 [Hz]	4096*/
	Chip_ADC_SetBurstCmd(LPC_ADC0, ENABLE);

	/*Enable ADC0 CH1 and its interrupt*/
	Chip_ADC_EnableChannel(LPC_ADC0, ADC_CH1, ENABLE);
	Chip_ADC_Int_SetChannelCmd(LPC_ADC0, ADC_CH1, ENABLE);

	/*===================== DMA Controller Configuration ================================*/
	Chip_GPDMA_Init(LPC_GPDMA);
	dma_ch_adc = Chip_GPDMA_GetFreeChannel(LPC_GPDMA, GPDMA_CONN_ADC_0);

	/*Enable DMA interrupt in the NVIC*/
	NVIC_ClearPendingIRQ(DMA_IRQn);
	NVIC_EnableIRQ(DMA_IRQn);

	/*===================== UART Configuration ================================*/

	/*UART	2 for debugging*/
	/* EDU-CIAA USART configuration */
	Chip_SCU_PinMuxSet(7, 1, SCU_MODE_FUNC6);  // [UM:17.4.1]
	Chip_SCU_PinMuxSet(7, 2, SCU_MODE_INBUFF_EN | SCU_MODE_FUNC6);

	/* USART3 peripheral configuration */
	Chip_UART_Init(LPC_USART2);
	Chip_UART_SetBaud(LPC_USART2, 9600);
	Chip_UART_TXEnable(LPC_USART2);

	/* Enable USART3 RBR interrupt */
	Chip_UART_IntEnable(LPC_USART2, UART_IER_RBRINT);

	/* Enable USART3 interrupt in the NVIC */
	NVIC_ClearPendingIRQ(USART2_IRQn);
	NVIC_EnableIRQ(USART2_IRQn);

	/*===================== SPI Configuration ================================*/

	Board_SSP_Init(LPC_SSP1);
	Chip_SSP_Init(LPC_SSP1);
	Chip_SSP_Enable(LPC_SSP1);

}

void startDMATransfer(void) {
	Chip_GPDMA_Transfer(LPC_GPDMA, dma_ch_adc,
			GPDMA_CONN_ADC_0, (uint32_t) &dma_buf,
			GPDMA_TRANSFERTYPE_P2M_CONTROLLER_DMA, N_SAMPLES);
}


/*==================[external functions definition]==========================*/


/* DMA interrupt handler triggered after N_SAMPLES conversions of ADC0_1 */
void DMA_IRQHandler(void) {
	portBASE_TYPE taskWoken;
	if (Chip_GPDMA_Interrupt(LPC_GPDMA, dma_ch_adc) == SUCCESS)
	{
		xSemaphoreGiveFromISR(dmaSemBin , &taskWoken);
	}
	portEND_SWITCHING_ISR(taskWoken);
}


static void task_FFT()
{
	/*	Once the data has been read and converted to float, this task calculates
	 * 	the float point FFT.
	 */

	xSemaphoreTake(fftSemBin , 0);
	while (1)
	{
		xSemaphoreTake(fftSemBin , portMAX_DELAY);

		arm_cfft_f32(&arm_cfft_sR_f32_len2048 ,fftInput ,FFT_MODE ,DO_BIT_REVERSE);

		/*FFT Module */
		arm_cmplx_mag_f32(fftInput, fftOutput, FFT_SIZE);



		xSemaphoreGive(dataReadySem);
	}
}

static void task_DMA()
{
	uint16_t i= 0;
	xSemaphoreTake(dmaSemBin , 0);
	while (1)
	{
		/* After the DMA transfers N_SAMPLES values from ADC0_CH1 to
		 * dma_buf[], copy them to adc_buf[] and start a new DMA transfer */

		xSemaphoreTake(dmaSemBin , portMAX_DELAY);
		if (i == 2*FFT_SIZE)		/*¿¿¿¿¿ 2*FFT_SIZE?????*/
		{
			i = 0;
			xSemaphoreGive(fftSemBin);
		}
		else
		{
			adc_Float_Conv(dma_buf, fftInput, FFT_SIZE, V_REF, i);
			i+=4;
			startDMATransfer();
		}
	}
}




static void taskmain(void * a) {
	int8_t key, last = 0;

	xSemaphoreTake(dataReadySem, 0);

	while (1) {

//		Board_LED_Set(0, true);
//		Board_LED_Set(3, false);

		startDMATransfer();

		/*Wait for fft and SD tasks*/
		xSemaphoreTake(dataReadySem, portMAX_DELAY);

		key = find_key();

		if (key==-1)
		{
			last = 0;
			continue;
		}

		last = key;
//		Board_LED_Set(0, false);
//		Board_LED_Set(3, true);

//		if (key != last)
//		{
		Chip_UART_Send(LPC_USART2, &last, 1);
//		Chip_UART_Send(LPC_USART2, &key, 1);
//		}

//		last = key;


	}
}

static int8_t find_key()
{
	int8_t index,key;

	index = vert_sweep();

	if (index == -1)
		return -1;

	key = hor_sweep(index);

	if (key == -1)
		return -1;

	return key;
}

static uint8_t vert_sweep()
{
	uint16_t i;
	uint8_t key_index;
	float32_t maxValue[4], peakValue;

	//	check for '1' at 697 Hz
	for (i=337, maxValue[0] = fftOutput[337]; i < 358; i++)
	{
		if (maxValue[0] < fftOutput[i])
		{
			maxValue[0] = fftOutput[i];
		}
	}



	//	check for '4' at 770 Hz
	for (i = 375, maxValue[1] = fftOutput[375]; i < 395; i++)
	{
		if (maxValue[1] < fftOutput[i])
		{
			maxValue[1] = fftOutput[i];
		}
	}

	//	check for '7' at 852 Hz
	for (i = 416, maxValue[2] = fftOutput[416]; i < 436; i++)
	{
		if (maxValue[2] < fftOutput[i])
		{
			maxValue[2] = fftOutput[i];
		}
	}

	//	check for '*' at 941 Hz
	for (i = 460, maxValue[3] = fftOutput[460]; i < 481; i++)
	{
		if (maxValue[3] < fftOutput[i])
		{
			maxValue[3] = fftOutput[i];
		}
	}

	if ((maxValue[0] < NOISE_LEVEL) && (maxValue[1] < NOISE_LEVEL) && (maxValue[2] < NOISE_LEVEL) && (maxValue[3] < NOISE_LEVEL))
	{
		return -1;
	}


	peakValue = maxValue[0];
	key_index = 0;
	//	Find the max value
	for (i=0; i<4; i++)
	{
		if (peakValue < maxValue[i])
		{
			peakValue=maxValue[i];
			key_index = i;
		}
	}

	return (key_index*4);
}

static uint8_t hor_sweep(uint8_t index)
{
	uint16_t i;
	uint8_t key, key_index;
	float32_t maxValue[4], peakValue;

	//	check for '1' at 1209 Hz
	for (i= 604-FREQUENCY_RES, maxValue[0] = fftOutput[604-FREQUENCY_RES]; i < 604+FREQUENCY_RES; i++)
	{
		if (maxValue[0] < fftOutput[i])
		{
			maxValue[0] = fftOutput[i];
		}
	}



	//	check for '2' at 1336 Hz
	for (i = 668-FREQUENCY_RES, maxValue[1] = fftOutput[668-FREQUENCY_RES]; i < 668+FREQUENCY_RES; i++)
	{
		if (maxValue[1] < fftOutput[i])
		{
			maxValue[1] = fftOutput[i];
		}
	}

	//	check for '3' at 1477 Hz
	for (i = 738-FREQUENCY_RES, maxValue[2] = fftOutput[738-FREQUENCY_RES]; i < 738+FREQUENCY_RES; i++)
	{
		if (maxValue[2] < fftOutput[i])
		{
			maxValue[2] = fftOutput[i];
		}
	}

	//	check for 'A' at 1633 Hz
	for (i = 816-FREQUENCY_RES, maxValue[3] = fftOutput[816-FREQUENCY_RES]; i < 816+FREQUENCY_RES; i++)
	{
		if (maxValue[3] < fftOutput[i])
		{
			maxValue[3] = fftOutput[i];
		}
	}

	if ((maxValue[0] < NOISE_LEVEL) && (maxValue[1] < NOISE_LEVEL) && (maxValue[2] < NOISE_LEVEL) && (maxValue[3] < NOISE_LEVEL))
	{
		return -1;
	}

	peakValue = maxValue[0];
	key_index = 0;
	//	Find the max value
	for (i=0; i<4; i++)
	{
		if (peakValue < maxValue[i])
		{
			peakValue=maxValue[i];
			key_index = i;
		}
	}

	key = dtmf_standard[key_index + index];

	return key;
}



int main(void) {
	initHardware();

	xTaskCreate(task_DMA, (const char*)"task_DMA", TASK_STACK_SIZE, 0, tskIDLE_PRIORITY+1, 0);
	xTaskCreate(task_FFT, (const char * )"task_FFT", TASK_STACK_SIZE, 0, tskIDLE_PRIORITY+1, 0);
	xTaskCreate(taskmain, (const char * )"taskmain", TASK_STACK_SIZE, 0, tskIDLE_PRIORITY+1, 0);



	dmaSemBin = xSemaphoreCreateBinary();
	fftSemBin = xSemaphoreCreateBinary();
	dataReadySem = xSemaphoreCreateBinary();


	vTaskStartScheduler();

	while (1) {
	}

}

/** @} doxygen end group definition */

/*==================[end of file]============================================*/
