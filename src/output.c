/** \addtogroup output
 ** @{ */

/*==================[inclusions]=============================================*/
#include "output.h"

/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/


/*==================[internal data definition]===============================*/

bool displayTuningNeeded(float32_t min ,float32_t frequency)
{

	/* I've already got the frequency of the guitar
	 * Compare it with notes.frequency
	 * Tolerance: 4%: It depends on the resolution 8000/2048
	 * For Lower frequencies (E = 82Hz)*/

	if (min >= frequency * 0.96
			&& min <= frequency * 1.04) {
//		Chip_UART_Send(LPC_USART3, "Tuned guitar!\n", 14);
//		LCD_GoToxy(0, 0);
//		LCD_Print("Tuned Guitar!");
		return true;
	}
	if (min < frequency * 0.96){
//		Chip_UART_Send(LPC_USART3, "Tuneful guitar: --->\n", 19);
//		LCD_GoToxy(0, 0);
//		LCD_Print("Hola!!");
//		LCD_GoToxy(0, 0);
//		LCD_Print("Hola!!");
		return false;
	}
	if (min > frequency * 1.04){
//		Chip_UART_Send(LPC_USART3, "Tuneful guitar: <---\n", 19);
//		LCD_GoToxy(0, 0);
//		LCD_Print("Hola!!");
//		LCD_GoToxy(0, 0);
//		LCD_Print("Hola!!");
		return false;
	}
	return true;
}

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/


/*==================[external functions definition]==========================*/


/** @} doxygen end group definition */

/*==================[end of file]============================================*/
