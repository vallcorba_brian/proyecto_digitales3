#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <netdb.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
//#include <iostream.h>
//#include <conio.h>
#include <time.h>
//#include <windows.h>

#define TCP_PORT 4000
#define MAXDATASIZE 10

#define MESSAGE_SIZE 10

int main(int argc, char *argv[])
{
		/*Declaracion de variables*/
		
		
		
		unsigned int data_sent = 0;
		char message[MAXDATASIZE];
		
		fd_set sendfds;
		
		int filefd;
		char read_buf[50];


		/*Variables TCP*/
        int Sockfd_TCP, numbytes;  /* Contendra el numero de bytes recibidos
                                 despues de llamar a recv() */
        char buf[MAXDATASIZE];  /* Buffer donde se reciben los datos */
        struct hostent *he;     /* Se utiliza para convertir el nombre del
									host a su direccion IP */
        struct sockaddr_in their_tcp_addr;  /* direccion del server donde se
                                        conectara */
		
		/********** INICIO DEL PROGRAMA  ********/

	
		/* Convertimos el nombre del host a su direccion IP, OJO con no poner una dirección valida! */
        if ((he=gethostbyname(argv[1])) == NULL)
        {
                perror("gethostbyname");
                exit(1);
        }

		/* Creamos el socket */
        if ((Sockfd_TCP = socket(AF_INET, SOCK_STREAM, 0)) == -1)
        {
                perror("socket");
                exit(1);
        }

		/* Establecemos their_tcp_addr con la direccion del server */
        their_tcp_addr.sin_family = AF_INET;
        their_tcp_addr.sin_port = htons(TCP_PORT);
        their_tcp_addr.sin_addr = *((struct in_addr *)he->h_addr);
        bzero(&(their_tcp_addr.sin_zero), 8);

		/* Intentamos conectarnos con el servidor */
        if (connect(Sockfd_TCP, (struct sockaddr *)&their_tcp_addr,
                           sizeof(struct sockaddr)) == -1)
        {

                perror("connect");
                exit(1);
        }
   
	while (1) {

	/* Recibimos los datos del servidor */
	        if ((numbytes=recv(Sockfd_TCP, read_buf, MAXDATASIZE, 0)) == -1) {
                	perror("recv");
                	exit(1);
        	}
	
		perror("RECIBIDO");	
		printf("%s",read_buf);
		fflush(stdin);
        }

}
