#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <unistd.h>
#include <sys/time.h>


// C library headers
#include <stdio.h>
#include <string.h>

// Linux headers
#include <fcntl.h> // Contains file controls like O_RDWR
#include <errno.h> // Error integer and strerror() function
#include <termios.h> // Contains POSIX terminal control definitions
#include <unistd.h>

#define TCP_PORT 4000
#define BACKLOG 10     
#define MAXBUFLEN 50
#define END_MESSAGE 'Z'

void handlerInt(int val);

static int flag;


void main() {
 	/*Declaracion de variables*/
	fd_set readfds;
	int time_passed = 0;
	int udp_port;	
	//char mensajito [20];			
	/*Variables TCP*/
	int sockfd_TCP;     /* El servidor escuchara por sockfd_TCP */
        int newfd_TCP;      /*  las transferencias de datos se realizar
                                mediante newfd_TCP */
        struct sockaddr_in my_tcp_addr;     /* contendra la direccion IP y el
                                        numero de puerto local */
        struct sockaddr_in their_tcp_addr;  /* Contendra la direccion IP y
                                        numero de puerto del cliente */
        int sin_size;   /* Contendra el tamanio de la escructura sockaddr_in */
	
	/********** INICIO DEL PROGRAMA  ********/
	/* addr_len contendra el tamanio de la estructura sockadd_in  y numbytes
	el numero de bytes recibidos */

	/*Abro dev/ttyusb1*/
	int fd;
	char tx_buf, last_rx;
	struct termios tty;
	
	int addr_len, numbytes;
	char buf[MAXBUFLEN];   /* Buffer de recepcion */

	int serial_port;



	
		signal(SIGINT, handlerInt);

	
		
        if ((sockfd_TCP = socket(AF_INET, SOCK_STREAM, 0)) == -1) /*Crea un socket
                                         y verifica si hubo algun error*/
        {
                perror("socket");
                exit(1);
        }

	/* Asignamos valores a la estructura my_tcp_addr para luego poder llamar a la
	funcion bind() */
        my_tcp_addr.sin_family = AF_INET;
        my_tcp_addr.sin_port = htons(TCP_PORT); /*debe convertirse a network byte
                                        order porque es enviado por la red*/
        my_tcp_addr.sin_addr.s_addr = INADDR_ANY;   /* automaticamente usa la
                                                IP local */
        bzero(&(my_tcp_addr.sin_zero), 8);  /* rellena con ceros el resto de la estructura */

        if ( bind(sockfd_TCP, (struct sockaddr *)&my_tcp_addr, sizeof(struct sockaddr)) == -1) {
        	perror("bind");
        	exit(1);
        }
	
        if (listen(sockfd_TCP, BACKLOG) == -1) {
                perror("listen");
                exit(1);
        }

        sin_size = sizeof(struct sockaddr_in);
	if ((newfd_TCP = accept(sockfd_TCP, (struct sockaddr *)&their_tcp_addr, &sin_size)) == -1) {
                        perror("accept");
                        //continue;                  
        }

	serial_port = open("/dev/ttyUSB1", O_RDWR);	

	memset(&tty, 0, sizeof tty);

	// Read in existing settings, and handle any error
	if(tcgetattr(serial_port, &tty) != 0) {
		printf("Error %i from tcgetattr: %s\n", errno, strerror(errno));
	}

	tty.c_cflag &= ~PARENB; // Clear parity bit, disabling parity (most common)
	tty.c_cflag &= ~CSTOPB; // Clear stop field, only one stop bit used in communication (most common)
	tty.c_cflag |= CS8; // 8 bits per byte (most common)
	tty.c_cflag &= ~CRTSCTS; // Disable RTS/CTS hardware flow control (most common)
	tty.c_cflag |= CREAD | CLOCAL; // Turn on READ & ignore ctrl lines (CLOCAL = 1)

	tty.c_lflag &= ~ICANON;
	tty.c_lflag &= ~ECHO; // Disable echo
	tty.c_lflag &= ~ECHOE; // Disable erasure
	tty.c_lflag &= ~ECHONL; // Disable new-line echo
	tty.c_lflag &= ~ISIG; // Disable interpretation of INTR, QUIT and SUSP
	tty.c_iflag &= ~(IXON | IXOFF | IXANY); // Turn off s/w flow ctrl
	tty.c_iflag &= ~(IGNBRK|BRKINT|PARMRK|ISTRIP|INLCR|IGNCR|ICRNL); // Disable any special handling of received bytes

	tty.c_oflag &= ~OPOST; // Prevent special interpretation of output bytes (e.g. newline chars)
	tty.c_oflag &= ~ONLCR; // Prevent conversion of newline to carriage return/line feed
	// tty.c_oflag &= ~OXTABS; // Prevent conversion of tabs to spaces (NOT PRESENT ON LINUX)
	// tty.c_oflag &= ~ONOEOT; // Prevent removal of C-d chars (0x004) in output (NOT PRESENT ON LINUX)

	tty.c_cc[VTIME] = 10;    // Wait for up to 1s (10 deciseconds), returning as soon as any data is received.
	tty.c_cc[VMIN] = 0;

	// Set in/out baud rate to be 9600
	cfsetispeed(&tty, B9600);
	cfsetospeed(&tty, B9600);

	last_rx = 0;
	        
	while(!flag) {		
	read(serial_port,&tx_buf,1);
	
	if (tx_buf == -1)
	{
		last_rx = 0;
		continue;
	}

	if (tx_buf != last_rx)
	{
		printf("%c\n",tx_buf);
		if (send(newfd_TCP, &tx_buf, 1, 0) == -1) 
		{
			perror("Error al enviar la direccion UDP\n");
		}
		tx_buf = last_rx;

	}
	
	}
tx_buf = END_MESSAGE;

send(newfd_TCP, &tx_buf, 1, 0);

sleep(5);

close(newfd_TCP); 
}


void handlerInt(int val)
{
	flag = 1;
}

