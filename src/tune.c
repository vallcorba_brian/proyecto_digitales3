/** \addtogroup tune
 ** @{ */

/*==================[inclusions]=============================================*/
#include "tune.h"

/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/
/**/
float32_t guitarTune(float32_t frequency ,float32_t* fftOutput, uint16_t Sz)
{
	uint16_t i,j;
	uint32_t max[GUITAR_TUNE_RANGE] = {0,0,0};
	float32_t guitar_tune[GUITAR_TUNE_RANGE];
	float32_t min;
	uint16_t minIndex;

	for (j = 0; j < GUITAR_TUNE_RANGE; j++)
	{
		//		max[j] = (float32_t) 1/Sz;
		for (i = 0; i < Sz/2; i++)
		{
			if (fftOutput[i] > fftOutput[max[j]]	&&
					i != max[0]		&&		i != max[1]	)
			{
				max[j] = i;
			}
		}
	}

	/* Compare *max with the frequency of the note to tune
	 * Find the most closer value to guitar_tune */
	for (i=0 ;i<GUITAR_TUNE_RANGE ;i++)
	{
		guitar_tune[i] = frequency - max[i];
		if (guitar_tune[i] < 0)
		{
			guitar_tune[i] = -1 * guitar_tune[i];
		}
	}


	min = guitar_tune[0];
	minIndex = 0;
	for (i = 0; i < GUITAR_TUNE_RANGE; i++) {
		if (guitar_tune[i] < min)
		{
			min = guitar_tune[i];
			minIndex = i;
		}

	}
	minIndex = max[minIndex];
	return minIndex;
}


/*==================[internal data definition]===============================*/



/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/


/*==================[external functions definition]==========================*/



/** @} doxygen end group definition */

/*==================[end of file]============================================*/
